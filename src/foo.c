/*
 * IEEE 754 format:
 * ================
 *
 * it's a set of representations of numerical values and symbols which
 * specifies how floating point numbers should be formatted.  every
 * floating-point number should be specified by:
 *
 *	 + a base (redix) `b`, which could be either 2 in case of a binary
 *	   representation, or 10 in case of a decimal one.
 *	 + a precision `p`
 *	 + an exponent range from `emin` to `emax`. (emin = 1 - emax)
 *
 * the format comprises:
 *
 *	 + finite numbers, described by three integers; a sign `s` (0 or 1),
 *	   a coefficient `c` have at max `p` digits when written in base `b`
 *	   (ie, rangine through 0 to b^p-1)
 *
 * the algorithm:
 * ==============
 *
 *
 */

/*	  Finds a homography between two sets of 2d points.

	Specifically, approximately minimize the weighted image plane
	error

	   \(min_A	sum_{X,y,w}	 w	||(A_12 x_) /  (A_3 x_1)  -	 y||^2\)

	where \(x_1\)=[x;1], \(A_{12}\) are the first two rows of A and \(A_3\) is the
	third row of A.
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/* int main(int argc, char *argv[]) { */

/*	return 0; */
/* } */
